FROM alpine:latest

RUN \
        mkdir -p /aws && \
        apk -Uuv add bash groff less python3 py3-pip git openssh-client docker jq unzip && \
        pip3 install awscli && \
        apk --purge -v del py-pip && \
rm /var/cache/apk/*

RUN mkdir -p /root/.ssh
RUN mkdir -p /usr/deployer
ADD ci.dockerfile /usr/deployer/ci.dockerfile
ADD aws-credentials /root/.aws/credentials
ADD bitbucket-key.pem /usr/deployer/bitbucket-key.pem
COPY common-config-files/* /usr/deployer/common-config-files/
ADD build /build
RUN chmod +x /build

WORKDIR /tmp
CMD /build

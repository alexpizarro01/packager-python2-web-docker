FROM php:5.6 as builder

RUN apt-get update
RUN apt-get install -y git gnupg2 unzip zlib1g-dev
RUN docker-php-ext-install bcmath zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY bitbucket-key.pem /root/.ssh/id_rsa
RUN chmod 400 /root/.ssh/id_rsa
RUN echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config

COPY app /build/app

WORKDIR /build/app

RUN composer install --no-dev --no-progress

FROM php:5.6-apache

RUN apt-get update
RUN apt-get install -y curl wget unzip git locales libedit-dev libreadline-dev zlib1g-dev libicu-dev libxml2-dev libcurl4-openssl-dev libmcrypt-dev libpng-dev libpq-dev
RUN pecl install redis-2.2.8
RUN docker-php-ext-install readline mbstring intl zip xml json curl mcrypt gd pgsql mysql mysqli
RUN docker-php-ext-enable redis

#Install the Locale es_CL)
RUN printf "es_CL.UTF-8 UTF-8\n" >> /etc/locale.gen && locale-gen

WORKDIR /var/www
RUN chown www-data:www-data /var/www
COPY --from=builder --chown=www-data:www-data /build/app /var/www

USER root:root
COPY common-config-files/docker-php-ext-redis.ini /usr/local/etc/php/conf.d/docker-php-ext-redis.ini
COPY common-config-files/site.conf /etc/apache2/sites-available/
COPY common-config-files/entrypoint.sh /entrypoint.sh

RUN a2dissite 000-default.conf && a2ensite site.conf
RUN a2enmod rewrite

EXPOSE 80
CMD ["/bin/bash", "/entrypoint.sh"]
